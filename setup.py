from setuptools import setup

setup(
    name='lotto_7_59',
    version='0.1',
    install_requires=[],
    packages=['lotto_7_59'],
    package_data={'lotto_7_59': ['*.py']},
    entry_points='''
        [console_scripts]
        lotto_7_59=lotto_7_59.shell:cli
    ''',
    test_suite='nose.collector',
    tests_require=['nose'],
)
