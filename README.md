How to use this program
==========

There are two ways to use this program:

1. pip install this package and import the module.
2. create an instance of the TicketChecker class

-- or --

1. use the python command line: python lotto_7_59.py followed by a space separated list of possible tickets:
Ex: > python lotto_7_59.py 1 42 4938532894754 1234567 472844278465445

How to test this program
==========

This program has two main tests and should work under most python frameworks as long as pip and tox have been installed.
The remaining packages are installed and run from within a virtual environment to prevent polution of the development system.

1. The standard PEP8 tests, which are tested via tox:

> tox -epep8

2. The standard unit tests.

> tox -epy27

Additional verbosity may assist in debugging by inserting the `-v` flag to tox.


Brief description of the program's logic
==========

Since it is considered bad pythonic form to add documentation in the code,
I have added the explanation of each funciton here:

The main program is split into 2 publically consumable functions.

def verify_tickets(self, list_of_tickets):
If you need more than one ticket, use this function.
It anticipates a list of strings that are decimal digits. Currently this function does not do validation on the inputs and assumes that the tickets are strings of decimal digits.

and

def verify_ticket(self, ticket):

If you need to verify a single ticket, use this function.
The arguments here are the string of numbers in the ticket. Currently this function does not do validation on the inputs and assumes that the ticket is a string of decimal digits.

Verify ticket does verify that the ticket is within a reasonable size being neither too short nor too long.

This function contains helpers to ensure that the ticket can be split in any reasonable combination of ways using the itertools.combinations function.

Details of that can be found here:

https://docs.python.org/2/library/itertools.html#itertools.combinations

We either then split the ticket by grabbing the next digit, or the next two digits and check the validity of this. We keep a running tally of all the numbers we've seen in a bit array and fail out if we see any number twice. We permute across a single ticket until we've either found a valid ticket or until we've exhausted all combinations.

With this saved bitfield, we have to potential to print the ticket numbers in successive order if desired.

Additional notes:

This program may work for additional ticket types by modifying the NUMBER_OF_PICKS field and MAX_NUMBER field. This has not been extensively tested.
