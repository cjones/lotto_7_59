Things that need to be improved
==========

- Add in argparse for help and instructions.
- Expand the possibility of the program to exist beyond a 2 digit number pick.
- Make the number of picks and the max number configurable on the command line.
- Add in better error message output/loggging
- Add in logic for prefered method output
- Add in specific logic for debug logs, or other verbose logging that may help
  the end user
- Add in the ability to choose the type of output the end user should get, either  in increasing order, or in order originaly present.
- Add in real unit tests
- Add in Sphinx requirement and test the Sphinx documentation.
- Test the actual package installation via "pip install -e ./lotto_7_59"
