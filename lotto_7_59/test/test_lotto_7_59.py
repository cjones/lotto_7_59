# vim: tabstop=4 shiftwidth=4 softtabstop=4
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import sys
import unittest

from lotto_7_59 import lotto_7_59


class TestLotto_7_59(unittest.TestCase):

    def setUp(self):
        self._stub_stdouts()

    def tearDown(self):
        self._out = None

    def _stub_stdouts(self):
        stdout = sys.stdout
        self._out = sys.stdout

        def cleanup():
            sys.stdout = stdout

        self.addCleanup(cleanup)

    @classmethod
    def setup_class(cls):
        cls._checker = lotto_7_59.TicketChecker()

    def _assert_equal(self, result, expected):
        assert result == expected, 'result: {} is not == b: {}'.format(
            result, expected
        )

    def test_given_list(self):
        # Test the input list given
        # List with known output:
        test_list = ["1", "42", "4938532894754", "1234567", "472844278465445"]
        self._checker.verify_tickets(test_list, out=self._out)
        result = self._out.getvalue()
        expected = ('4938532894754 -> 49 38 53 28 9 47 54 \n'
                    '1234567 -> 1 2 3 4 5 6 7 \n')
        self._assert_equal(result, expected)

    def test_valid_single(self):
        # Test a single valid case
        test_single = '7654321'
        self._checker.verify_ticket(test_single, out=self._out)
        result = self._out.getvalue()
        expected = '7654321 -> 7 6 5 4 3 2 1 \n'
        self._assert_equal(result, expected)

    def test_invalid_single(self):
        # Test a single invalid case, we expect nothing returned
        test_single = '77654321'
        self._checker.verify_ticket(test_single, out=self._out)
        result = self._out.getvalue()
        expected = ''
        self._assert_equal(result, expected)

    @unittest.skip('Need to implement')
    def test_valid_list(self):
        # Test a list of valid
        pass

    @unittest.skip('Need to implement')
    def test_invalid_list(self):
        # Test a list of invalid
        pass

    def test_palendrome_single(self):
        # Test a single palendrome
        # Ex. 12, 13, 14, 33, 41, 31, 21
        test_single = '12131433413121'
        self._checker.verify_ticket(test_single, out=self._out)
        result = self._out.getvalue()
        expected = '12131433413121 -> 12 13 14 33 41 31 21 \n'
        self._assert_equal(result, expected)

    def test_palendrome_list(self):
        # Test a list of palendromes
        # Ex. 12, 13, 14, 33, 41, 31, 21
        test_list = ['12131433413121', '12131444413121', '32131444413123',
                     '32132444423123']
        self._checker.verify_tickets(test_list, out=self._out)
        result = self._out.getvalue()
        expected = ('12131433413121 -> 12 13 14 33 41 31 21 \n'
                    '12131444413121 -> 12 13 14 44 41 31 21 \n'
                    '32131444413123 -> 32 13 14 44 41 31 23 \n'
                    '32132444423123 -> 32 13 24 44 42 31 23 \n'
                    )
        self._assert_equal(result, expected)

    def test_semi_palendrome_list(self):
        # Test a list of semi-palendromes
        # These tests should be near palendromes and work both forwards and in
        # reverse
        # Ex. 11, 22, 32, 33, 44, 54, 55
        test_single = '11223233445455'
        self._checker.verify_ticket(test_single, out=self._out)
        result = self._out.getvalue()
        expected = '11223233445455 -> 11 22 32 33 44 54 55 \n'
        self._assert_equal(result, expected)
        test_single_inv = test_single[::-1]
        self._checker.verify_ticket(test_single_inv, out=self._out)
        result = self._out.getvalue()
        expected = ('11223233445455 -> 11 22 32 33 44 54 55 \n'
                    '55454433232211 -> 55 45 44 33 23 22 11 \n'
                    )
        self._assert_equal(result, expected)

    @unittest.skip('Need to implement')
    def test_semi_palendrome_single(self):
        # Test semi-palendromes
        # Ex. 12, 13, 14, 33, 41, 31, 21
        pass

    def test_empty_entry(self):
        test_single = ''
        self._checker.verify_ticket(test_single, out=self._out)
        result = self._out.getvalue()
        expected = ''
        self._assert_equal(result, expected)

    def test_empty_list(self):
        test_list = []
        self._checker.verify_tickets(test_list, out=self._out)
        result = self._out.getvalue()
        expected = ''
        self._assert_equal(result, expected)
