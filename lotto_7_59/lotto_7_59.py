#!/usr/bin/python
import itertools
import sys


NUMBER_OF_PICKS = 7
# TODO(cjones): Current logic ties MAX_NUMBER to be no greater than 2 digits
MAX_NUMBER = 59


def main(args):
    ticket_checker = TicketChecker()
    ticket_list = args
    ticket_checker.verify_tickets(ticket_list)


class TicketChecker(object):

    def __init__(self):
        self._initialize()

    def _initialize(self):
        self._used_numbers = 0
        self._output_numbers = ""

    def verify_tickets(self, list_of_tickets, out=sys.stdout):
        """Verifies that validity of several tickets.

        :param list_of_tickets: list of strings representing the tickets.
            It is currently assumed that all tickets are consist of decimal
            digits as these are not currently validated.
        :raises None at present.
        :return: None. But it prints the values of the ticket to screen if it
            is valid.
        """

        for ticket in list_of_tickets:
            self.verify_ticket(ticket, out=out)

    def verify_ticket(self, ticket, out=sys.stdout):
        """Verifies that validity of a single ticket.

        :param ticket: string representing the tickets.
            It is currently assumed that the ticket consists of decimal digits
            as these are not currently validated.
        :raises None at present.
        :return: None. But it prints the value of the ticket to screen if it is
             valid.
        """

        ticket_len = len(ticket)
        if ticket_len < NUMBER_OF_PICKS or ticket_len > NUMBER_OF_PICKS * 2:
            # TODO(cjones): It was not required of the assignment to print
            # out this information, but it may be nice in the future
            # print "Ticket length too short or too long for ticket: " + ticket
            return
        return_val = self._verify_tickets_with_splits(ticket)
        if return_val:
            self._print_ticket_and_numbers(ticket, out=out)

    def _verify_tickets_with_splits(self, ticket):
        ticket_length = len(ticket)
        # TODO(cjones): This logic ties the game to go no larger than 99
        number_of_doubles = ticket_length - NUMBER_OF_PICKS
        number_of_singles = ticket_length - number_of_doubles * 2
        splits = itertools.combinations(range(NUMBER_OF_PICKS),
                                        int(number_of_singles))
        for split in splits:
            self._initialize()
            valid_ticket = self._verify_ticket_with_split(ticket, split)
            if valid_ticket:
                break
        return valid_ticket

    def _verify_ticket_with_split(self, ticket, split):
        cur_pos = 0
        valid_ticket = False
        for i in range(NUMBER_OF_PICKS):
            if i in split:
                new_pos = cur_pos + 1
            else:
                new_pos = cur_pos + 2

            number = int(ticket[cur_pos:new_pos])
            cur_pos = new_pos
            if self._check_and_add_number(number):
                break
            valid_ticket = (i == NUMBER_OF_PICKS - 1)
        return valid_ticket

    def _check_and_add_number(self, number):
        if number == 0 or number > MAX_NUMBER:
            # TODO(cjones): It was not required of the assignment to print
            # out this information, but it may be nice in the future for
            # logging or output
            # print "This number is outside the allowed range: " + str(number)
            return 1
        correct_bit = 1 << int(number) - 1
        if (self._used_numbers & correct_bit) != 0:
            # TODO(cjones): It was not required of the assignment to print
            # out this information, but it may be nice in the future for
            # logging or output
            # print "This number has already been used: " + str(number)
            return 1
        self._output_numbers = self._output_numbers + str(number) + " "
        self._used_numbers = self._used_numbers | correct_bit

    def _print_ticket_and_numbers(self, ticket, out=sys.stdout):
        out.write(ticket + " -> " + self._output_numbers + "\n")

    def _print_ticket_and_numbers_in_order(self, ticket, out=sys.stdout):
        numbers = ""
        for bit in range(MAX_NUMBER):
            bit_pos = 1 << int(bit)
            if (self._used_numbers & int(bit_pos)) != 0:
                numbers = numbers + str(bit + 1) + " "
        out.write(ticket + " -> " + numbers + "\n")


if __name__ == "__main__":
    main(sys.argv[1:])
